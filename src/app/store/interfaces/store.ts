import { Layer } from '@map/interfaces/layer';
import { LayerElement } from '@map/interfaces/layer-element';
import { Trail } from '@map/interfaces/trail';

export interface Store {
  heatmap: any;
  layerElements: LayerElement[];
  layers: Layer[];
  map: any;
  markers: any;
  popup: any;
  splashScreen: any;
  trails: Trail[];
}
