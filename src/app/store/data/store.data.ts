export const store = {
  heatmap: {
    id: 'heatmap',
    active: false,
    class: '',
    params: {
      coverage: 1,
      elevationScale: 100,
      radius: 1000,
      upperPercentile: 100
    },
    props: {
      colourRange: [
        [1, 152, 189],
        [73, 227, 206],
        [216, 254, 181],
        [254, 237, 177],
        [254, 173, 84],
        [209, 55, 78]
      ],
      coverage: 1,
      elevationRange: [0, 3000],
      elevationScale: 100,
      extruded: true,
      material: {
        ambient: 0.6,
        diffuse: 0.6,
        shininess: 50,
        specularColor: [200, 200, 200]
      },
      opacity: 1,
      radius: 1000,
      upperPercentile: 100
    },
    settings: {
      bearing: -30,
      center: [-1.8, 52.0],
      container: 'heatmap',
      initial: {
        bearing: -30,
        center: [-1.8, 52.0],
        pitch: 50,
        zoom: 6.5
      },
      doubleClickZoom: true,
      maxZoom: 11,
      minZoom: 2,
      pitch: 50,
      style: 'mapbox://styles/mapbox/dark-v10',
      zoom: 6.5
    }
  },
  layerElements: [
    {
      id: 'satellite',
      icon_id: 'satellite-icon',
      active: false,
      class: '',
      name: 'Satellite',
      src: 'assets/satellite.png',
      height: 20,
      width: 20
    },
    {
      id: 'biosphere',
      icon_id: 'biosphere-icon',
      active: true,
      class: 'active',
      name: 'Biosphere',
      src: 'assets/biosphere.png',
      height: 16,
      width: 16
    },
    {
      id: 'office',
      icon_id: 'office-icon',
      active: false,
      class: '',
      name: 'Office',
      src: 'assets/office.png',
      height: 20,
      width: 18
    },
    {
      id: 'places',
      icon_id: 'places-icon',
      active: false,
      class: '',
      name: 'Places',
      src: 'assets/places.png',
      height: 20,
      width: 18
    },
    {
      id: 'trails',
      icon_id: 'trails-icon',
      active: false,
      class: '',
      name: 'Trails',
      src: 'assets/trails.png',
      height: 20,
      width: 18
    },
    {
      id: 'heatmap',
      icon_id: 'heatmap-icon',
      name: 'Deck.GL',
      src: 'assets/heatmap.png',
      height: 18,
      width: 18
    },
    {
      id: 'logout',
      icon_id: 'logout-icon',
      name: 'Logout',
      src: 'assets/logout.png',
      height: 18,
      width: 18
    }
  ],
  layers: {
    biosphere: {
      active: true
    },
    trails: {
      active: false
    }
  },
  map: {
    id: 'map',
    accessToken: null,
    active: false,
    class: '',
    hillshade: {
      id: 'hillshading',
      active: false,
      index: 'waterway-river-canal-shadow',
      layer: {
        type: 'raster-dem',
        url: 'mapbox://mapbox.terrain-rgb'
      },
      source: 'dem',
      type: 'hillshade'
    },
    navigationControl: {
      position: 'top-left'
    },
    settings: {
      bearing: 0,
      bounds: null,
      center: [-76.3, 44.45],
      container: 'map',
      doubleClickZoom: false,
      maxZoom: 11,
      minZoom: 2,
      pitch: 0,
      style: 'mapbox://styles/mapbox/outdoors-v10',
      zoom: 9
    },
    styles: {
      active: 'outdoors',
      outdoors: {
        id: 'outdoors',
        active: true,
        type: 'vector',
        url: 'mapbox://styles/mapbox/outdoors-v10'
      },
      satellite: {
        id: 'satellite',
        active: false,
        type: 'vector',
        url: 'mapbox://styles/mapbox/satellite-v9'
      }
    }
  },
  markers: {
    office: {
      active: false
    },
    places: {
      active: false
    },
    trails: {
      active: false
    }
  },
  popup: {
    layer: {
      active: false
    },
    marker: {
      active: false,
      layer: null,
      name: null
    }
  },
  splashScreen: {
    active: false,
    class: ''
  },
  trails: [
    {
      name: 'Select Trail'
    },
    {
      name: 'Blue Mountain',
      active: false,
      center: [-76.04, 44.508],
      zoom: 11
    },
    {
      name: 'Charleston Lake',
      active: false,
      center: [-76.04, 44.508],
      zoom: 11
    },
    {
      name: 'Lemoine Point',
      active: false,
      center: [-76.61, 44.223],
      zoom: 11
    },
    {
      name: 'Lyn Valley',
      active: false,
      center: [-75.75, 44.575],
      zoom: 11
    },
    {
      name: 'Mac Johnson',
      active: false,
      center: [-75.75, 44.575],
      zoom: 11
    },
    {
      name: "Seeley's Bay",
      active: false,
      center: [-76.22, 44.485],
      zoom: 11
    }
  ]
};
