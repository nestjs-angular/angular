import { Injectable } from '@angular/core';
import { Model, ModelFactory } from '@angular-extensions/model';
import { Observable } from 'rxjs';

import { HeatmapSettings } from '@heatmap/interfaces/heatmap-settings';
import { MapSettings } from '@map/interfaces/map-settings';
import { Store } from '@store/interfaces/store';
import { Trail } from '@map/interfaces/trail';

import { store } from '@store/data/store.data';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private model: Model<Store>;
  private state: any;

  store$: Observable<Store>;

  constructor(private modelFactory: ModelFactory<Store>) {
    this.state = store;
  }

  createStore(): void {
    this.model = this.modelFactory.create(this.state);
    this.store$ = this.model.data$;
  }

  private getState(): Store {
    return this.model.get();
  }

  private setState(state: Store): void {
    this.model.set(state);
    // this.saveStore(state);
  }
  /* for debugging purposes only
  private saveStore(state: Store): void {
    sessionStorage.setItem('store', JSON.stringify(state));
  }
  */
  resetHeatmapSettings(): void {
    const state: Store = this.getState();
    this.setHeatmapSettings(state.heatmap.settings.initial);
  }

  setHeatmapActive(): void {
    const state: Store = this.getState();
    state.heatmap.active = !state.heatmap.active;
    state.heatmap.active ? (state.heatmap.class = 'active') : (state.heatmap.class = '');
    this.setState(state);
  }

  setHeatmapParams(param: string, value: number): void {
    const state: Store = this.getState();
    state.heatmap.params[param] = value;
    this.setState(state);
  }

  setHeatmapSettings(settings: HeatmapSettings): void {
    const state: Store = this.getState();
    Reflect.ownKeys(settings).forEach((key: string): void => {
      state.heatmap.settings[key] = settings[key];
    });
    this.setState(state);
  }

  setHillshadeActive(): void {
    const state: Store = this.getState();
    state.map.hillshade.active = !state.map.hillshade.active;
    this.setState(state);
  }

  setLayerElementActive(i: number): void {
    const state: Store = this.getState();
    state.layerElements[i].active = !state.layerElements[i].active;
    state.layerElements[i].active ? (state.layerElements[i].class = 'active') : (state.layerElements[i].class = '');
    this.setState(state);
  }

  setLayerPopupActive(): void {
    const state: Store = this.getState();
    state.popup.layer.active = !state.popup.layer.active;
    this.setState(state);
  }

  setLayerActive(id: string): void {
    const state: Store = this.getState();
    state.layers[id].active = !state.layers[id].active;
    this.setState(state);
  }

  setMapActive(): void {
    const state: Store = this.getState();
    state.map.active = !state.map.active;
    state.map.active ? (state.map.class = 'active') : (state.map.class = '');
    this.setState(state);
  }

  setMapSettings(settings: MapSettings): void {
    const state: Store = this.getState();
    Reflect.ownKeys(settings).forEach((key: string): void => {
      state.map.settings[key] = settings[key];
    });
    this.setState(state);
  }

  setMapStyleActive(): void {
    let id: string;
    const state: Store = this.getState();
    state.map.styles[state.map.styles.active].active = !state.map.styles[state.map.styles.active].active;
    state.map.styles[state.map.styles.active].id === state.map.styles.outdoors.id
      ? (id = state.map.styles.satellite.id)
      : (id = state.map.styles.outdoors.id);
    state.map.styles.active = id;
    state.map.styles[id].active = !state.map.styles[id].active;
    this.setState(state);
  }

  setMapboxAccessToken(accessToken: string): void {
    const state: Store = this.getState();
    state.map.accessToken = accessToken;
    this.setState(state);
  }

  setMarkerActive(id: string): void {
    const state: Store = this.getState();
    state.markers[id].active = !state.markers[id].active;
    this.setState(state);
  }

  setMarkerPopupActive(layer: string, name: string): void {
    const state: Store = this.getState();
    state.popup.marker.active = !state.popup.marker.active;
    state.popup.marker.layer = layer;
    state.popup.marker.name = name;
    this.setState(state);
  }

  setSplashScreenActive(): void {
    const state: Store = this.getState();
    state.splashScreen.active = !state.splashScreen.active;
    state.splashScreen.active ? (state.splashScreen.class = 'active') : (state.splashScreen.class = '');
    this.setState(state);
  }

  setTrailActive(i: number): void {
    const state: Store = this.getState();
    state.trails.forEach((trail: Trail, j: number): void => {
      i === j ? (trail.active = true) : (trail.active = false);
    });
    this.setState(state);
  }
}
