import { EventEmitter, Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AuthorizationService } from '@access/authorization/services/authorization.service';
import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class SessionGuard implements CanActivate {
  private eventEmitter: EventEmitter<any>;

  constructor(private authorizationService: AuthorizationService, private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  canActivate(): any {
    if (this.authorizationService.hasSessionExpired()) {
      return this.eventEmitter.emit(['logout']);
    }

    return true;
  }
}
