import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AppService } from '@app/services/app.service';
import { AuthorizationService } from '@access/authorization/services/authorization.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private appService: AppService, private authorizationService: AuthorizationService) {}

  canActivate(): any {
    if (!this.authorizationService.hasSessionExpired()) {
      return this.appService.loadApp();
    }

    return true;
  }
}
