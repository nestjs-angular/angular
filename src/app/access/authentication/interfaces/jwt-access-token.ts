export interface JwtAccessToken {
  accessToken: string;
  expiresIn: number;
}
