import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient) {}

  async get(url: string, options?: any): Promise<any> {
    return await this.http.get<any>(url, options).toPromise();
  }

  async post(url: string, body: any, options?: any): Promise<any> {
    return await this.http.post<any>(url, body, options).toPromise();
  }
}
