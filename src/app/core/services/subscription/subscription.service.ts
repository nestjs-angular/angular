import { EventEmitter, Injectable } from '@angular/core';
import { NavigationStart, Router, RouterEvent } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Layer } from '@map/interfaces/layer';
import { LayerElement } from '@map/interfaces/layer-element';
import { Store } from '@store/interfaces/store';
import { Trail } from '@map/interfaces/trail';

import { AuthorizationService } from '@access/authorization/services/authorization.service';
import { DataService } from '@data/services/data.service';
import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';
import { HeatmapService } from '@heatmap/services/heatmap.service';
import { LayerService } from '@map/services/layer/layer.service';
import { LayerUIService } from '@map/services/layerUI/layer-ui.service';
import { MapService } from '@map/services/map.service';
import { MarkerService } from '@map/services/marker/marker.service';
import { PopupService } from '@map/services/popup/popup.service';
import { StoreService } from '@store/services/store.service';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  private eventEmitter: EventEmitter<any>;
  private state: Store;

  constructor(
    private router: Router,
    private authorizationService: AuthorizationService,
    private dataService: DataService,
    private eventEmitterService: EventEmitterService,
    private heatmapService: HeatmapService,
    private layerService: LayerService,
    private layerUIService: LayerUIService,
    private mapService: MapService,
    private markerService: MarkerService,
    private popupService: PopupService,
    private storeService: StoreService
  ) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  setSubscriptions(): void {
    const setEvents: Subscription = this.eventEmitter.subscribe({
      next: (params: any[]): void => {
        const event: string = params[0];

        switch (event) {
          case 'addLayers':
            this.layerService.layers.forEach((layer: Layer): void => {
              this.mapService.addLayer(layer);
            });
            break;

          case 'addLayerPopup':
            this.popupService.addLayerPopup(params[1]);
            break;

          case 'addMarkerPopup':
            this.popupService.addMarkerPopup(params[1], params[2]);
            break;

          case 'addToMap':
            this.mapService.addToMap(params[1]);
            break;

          case 'loadData':
            this.dataService.loadData();
            break;

          case 'loadFeatures':
            if (this.state.map.hillshade.active) {
              this.eventEmitter.emit(['setHillShadeActive']);
            }
            this.eventEmitter.emit(['addLayers']);
            this.eventEmitter.emit(['showMarkers']);
            this.eventEmitter.emit(['setSplashScreenActive']);
            break;

          case 'loadHeatmap':
            this.eventEmitter.emit(['showMarkers']);
            this.heatmapService.loadMap();
            break;

          case 'loadMap':
            this.mapService.loadMap();
            break;

          case 'logout':
            this.authorizationService.unsetSession();
            break;

          case 'removePopup':
            this.popupService.removePopup(params[1]);
            break;

          case 'resetHeatmapParams':
            this.heatmapService.resetParams();
            break;

          case 'resetHeatmapSettings':
            this.storeService.resetHeatmapSettings();
            this.heatmapService.resetSettings();
            break;

          case 'selectLayer':
            {
              const id: string = params[1];
              const i: number = this.state.layerElements.findIndex((layer: LayerElement): boolean => layer.id === id);
              if (id !== 'heatmap' && id !== 'logout') {
                this.eventEmitter.emit(['setLayerElementActive', i]);
              }
              this.layerUIService.selectLayer(this.state.layerElements[i]);
            }
            break;

          case 'selectTrail':
            {
              const name: string = params[1];
              const i: number = this.state.trails.findIndex((trail: Trail): boolean => trail.name === name);
              this.eventEmitter.emit(['setTrailActive', i]);
              this.mapService.flyTo(this.state.trails[i]);
            }
            break;

          case 'setHeatmapActive':
            this.storeService.setHeatmapActive();
            break;

          case 'setHeatmapData':
            this.heatmapService.data = params[1];
            break;

          case 'setHeatmapParams':
            this.storeService.setHeatmapParams(params[1], params[2]);
            break;

          case 'setHeatmapSettings':
            this.storeService.setHeatmapSettings(params[1]);
            break;

          case 'setHillShadeActive':
            this.storeService.setHillshadeActive();
            break;

          case 'setLayerActive':
            this.layerService.setLayerActive(params[1]);
            this.storeService.setLayerActive(params[1]);
            break;

          case 'setLayerVisibility':
            this.mapService.setLayerVisibility(params[1]);
            break;

          case 'setLayerElementActive':
            this.storeService.setLayerElementActive(params[1]);
            break;

          case 'setLayerPopupActive':
            this.storeService.setLayerPopupActive();
            break;

          case 'setLayers':
            this.layerService.setLayers(params[1]);
            break;

          case 'setMapActive':
            this.storeService.setMapActive();
            break;

          case 'setMapSettings':
            this.storeService.setMapSettings(params[1]);
            break;

          case 'setMapStyle':
            this.mapService.setMapStyle();
            break;

          case 'setMapStyleActive':
            this.storeService.setMapStyleActive();
            break;

          case 'setMapboxAccessToken':
            this.storeService.setMapboxAccessToken(params[1]);
            break;

          case 'setMarkerActive':
            this.storeService.setMarkerActive(params[1]);
            break;

          case 'setMarkerPopupActive':
            this.storeService.setMarkerPopupActive(params[1], params[2]);
            break;

          case 'setMarkers':
            this.markerService.setMarkers(params[1], params[2]);
            break;

          case 'setRoutePath':
            this.router.navigate([params[1]]);
            break;

          case 'setSplashScreenActive':
            this.storeService.setSplashScreenActive();
            break;

          case 'setTrailActive':
            this.storeService.setTrailActive(params[1]);
            break;

          case 'showMarkers':
            this.markerService.showMarkers();
            break;

          case 'toggleMarkers':
            this.markerService.toggleMarkers(params[1]);
            break;
        }
      },
      error: (err: Error): void => {
        console.error('setEvents Failed:\n', err);
      },
      complete: (): void => {
        setEvents.unsubscribe();
      }
    });

    const setRoutes: Subscription = this.router.events
      .pipe(
        filter((evt: RouterEvent): boolean => {
          return evt instanceof NavigationStart;
        })
      )
      .subscribe({
        next: (evt: NavigationStart): any => {
          if (this.authorizationService.hasSessionExpired()) {
            return true;
          }

          if (this.state.map.accessToken && evt.url !== '/') {
            if (!this.state.splashScreen.active) {
              this.eventEmitter.emit(['setSplashScreenActive']);
            }

            if (evt.url === '/map') {
              if (this.state.heatmap.active) {
                this.eventEmitter.emit(['setHeatmapActive']);
              }

              if (!this.state.map.active) {
                this.eventEmitter.emit(['setMapActive']);
              }

              return setTimeout((): void => this.eventEmitter.emit(['loadMap']));
            }

            if (evt.url === '/heatmap') {
              if (this.state.map.active) {
                this.eventEmitter.emit(['setMapActive']);
              }

              if (!this.state.heatmap.active) {
                this.eventEmitter.emit(['setHeatmapActive']);
              }

              return setTimeout((): void => this.eventEmitter.emit(['loadHeatmap']));
            }
          }

          return true;
        },
        error: (err: Error): void => {
          console.error('setRoutes Failed:\n', err);
        },
        complete: (): void => {
          setRoutes.unsubscribe();
        }
      });

    const setState: Subscription = this.storeService.store$.subscribe({
      next: (state: Store): void => {
        this.state = state;
        this.heatmapService.state = state;
        this.mapService.state = state;
      },
      error: (err: Error): void => {
        console.error('setState Failed:\n', err);
      },
      complete: (): void => {
        setState.unsubscribe();
      }
    });
  }
}
