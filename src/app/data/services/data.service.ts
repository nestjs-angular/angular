import { EventEmitter, Injectable } from '@angular/core';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import * as fetch from 'd3-fetch';
import { FeatureCollection } from 'geojson';

import { Urls } from '@core/enums/urls.enum';

import { Layer } from '@map/interfaces/layer';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';
import { HttpService } from '@core/services/http/http.service';

import { layers } from '@data/data/layer/layer.data';
import { markers } from '@data/data/marker/marker.data';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private eventEmitter: EventEmitter<any>;
  private layers: any[];
  private markers: any[];

  constructor(private eventEmitterService: EventEmitterService, private httpService: HttpService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
    this.layers = layers;
    this.markers = markers;
  }

  loadData(): void {
    this.getHeatmapData();
    this.getMapData();
  }

  private getHeatmapData(): void {
    fetch
      .csv('https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/3d-heatmap/heatmap-data.csv')
      .then((data: any[]): void => {
        if (data.length) {
          return this.eventEmitter.emit(['setHeatmapData', data]);
        }

        console.error('Data Error:\n', data);
      })
      .catch((err: Error): void => {
        console.error('getHeatmapData Failed:\n', err);
      });
  }

  private async http(params: HttpParams): Promise<FeatureCollection> {
    return await this.httpService.get(Urls.GEOJSON_ENDPOINT, { params });
  }

  private getMapData(): void {
    let params: HttpParams;

    this.layers.forEach((layer: { fields: string; layer: { id: string } }, i: number): void => {
      params = new HttpParams().set('fields', layer.fields).set('table', layer.layer.id);

      this.http(params)
        .then((fc: FeatureCollection): void => {
          if (fc.features.length) {
            const layer: Layer = this.layers[i].layer;
            layer.source.data = fc;
            return this.eventEmitter.emit(['setLayers', layer]);
          }

          console.log('No Features Found:\n', fc);
        })
        .catch((err: HttpErrorResponse): void => {
          console.error('http Failed:\n', err);
        });
    });

    this.markers.forEach((marker: { fields: string; id: string }): void => {
      params = new HttpParams().set('fields', marker.fields).set('table', marker.id);

      this.http(params)
        .then((fc: FeatureCollection): void => {
          if (fc.features.length) {
            return this.eventEmitter.emit(['setMarkers', fc, marker.id]);
          }

          console.log('No Features Found:\n', fc);
        })
        .catch((err: HttpErrorResponse): void => {
          console.error('http Failed:\n', err);
        });
    });
  }
}
