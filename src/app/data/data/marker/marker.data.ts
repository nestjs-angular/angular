export const markers = [
  {
    id: 'office',
    fields: 'name, description, geom'
  },
  {
    id: 'places',
    fields: 'name, description, geom'
  },
  {
    id: 'trails',
    fields: 'name, description, lat, lng, geom'
  }
];
