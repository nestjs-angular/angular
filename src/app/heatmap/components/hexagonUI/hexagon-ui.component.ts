import { EventEmitter, Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';
import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-hexagon-ui',
  styleUrls: ['hexagon-ui.component.scss'],
  templateUrl: 'hexagon-ui.component.html',
  animations: [
    trigger('fade', [
      state(
        'fade-out',
        style({
          opacity: 0,
          transform: 'translateX(0) scale(0)'
        })
      ),
      state(
        'fade-in',
        style({
          opacity: 1,
          transform: 'translateX(0) scale(1)'
        })
      ),
      transition('fade-out => fade-in', animate(1500))
    ])
  ]
})
export class HexagonUIComponent implements OnInit {
  private eventEmitter: EventEmitter<any>;

  fade = 'fade-out';

  constructor(private eventEmitterService: EventEmitterService, public storeService: StoreService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  ngOnInit(): void {
    setTimeout((): string => (this.fade = 'fade-in'));
  }

  resetCoords(): void {
    this.eventEmitter.emit(['resetHeatmapSettings']);
  }

  resetParams(): void {
    this.eventEmitter.emit(['resetHeatmapParams']);
  }

  returnTrails(): void {
    this.eventEmitter.emit(['setRoutePath', '/map']);
  }

  logout(): void {
    this.eventEmitter.emit(['logout']);
  }
}
