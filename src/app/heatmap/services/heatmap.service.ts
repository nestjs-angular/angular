/*
  URL: https://gist.github.com/Pessimistress/1a4f3f5eb3b882ab4dd29f8ac122a7be
  Title: deck.gl + Mapbox HexagonLayer Example
  Author: Xiaoji Chen (@pessimistress)
  Data URL: https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/3d-heatmap/heatmap-data.csv
  Data Source: https://data.gov.uk
*/
import { EventEmitter, Injectable } from '@angular/core';
import { HexagonLayer } from '@deck.gl/aggregation-layers';
import { MapboxLayer } from '@deck.gl/mapbox';
import { Map, NavigationControl } from 'mapbox-gl';

import { HeatmapSettings } from '@heatmap/interfaces/heatmap-settings';
import { Store } from '@store/interfaces/store';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class HeatmapService {
  private eventEmitter: EventEmitter<any>;
  private heatmap: MapboxLayer;
  private heatmapSettings: HeatmapSettings;
  private map: Map;

  data: [{ lng: string; lat: string }];
  state: Store;

  constructor(private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
    this.heatmapSettings = {
      bearing: null,
      center: null,
      pitch: null,
      zoom: null
    };
  }

  loadMap() {
    this.map = new Map(this.state.heatmap.settings)
      .addControl(new NavigationControl(), this.state.map.navigationControl.position)
      .on('load', (): void => {
        this.setHeatmap();
      })
      .on('idle', (): void => {
        this.setSettings();
      });
  }

  private setHeatmap() {
    this.heatmap = new MapboxLayer({
      id: this.state.heatmap.id,
      type: HexagonLayer,
      colorRange: this.state.heatmap.props.colourRange,
      coverage: this.state.heatmap.params.coverage,
      data: this.data,
      elevationRange: this.state.heatmap.props.elevationRange,
      elevationScale: this.state.heatmap.params.elevationScale,
      extruded: this.state.heatmap.props.extruded,
      getPosition: (d: any): any[] => [+d.lng, +d.lat],
      material: this.state.heatmap.props.material,
      opacity: this.state.heatmap.props.opacity,
      radius: this.state.heatmap.params.radius,
      upperPercentile: this.state.heatmap.params.upperPercentile
    });

    this.setParams();
    this.addLayer();
  }

  private addLayer() {
    this.map.addLayer(this.heatmap);
    this.setLayerVisibility();
  }

  private setLayerVisibility() {
    this.eventEmitter.emit(['setSplashScreenActive']);
    this.map.setLayoutProperty(this.state.heatmap.id, 'visibility', 'visible');
  }

  private setParams() {
    Reflect.ownKeys(this.state.heatmap.params).forEach((param): void => {
      document.getElementById(param as string).oninput = (evt: any): void => {
        this.eventEmitter.emit(['setHeatmapParams', param, +evt.target.value]);
        this.heatmap.setProps({ [param]: this.state.heatmap.params[param] });
      };
    });
  }

  private setSettings() {
    this.heatmapSettings.bearing = this.map.getBearing();
    this.heatmapSettings.center = this.map.getCenter().toArray();
    this.heatmapSettings.pitch = this.map.getPitch();
    this.heatmapSettings.zoom = this.map.getZoom();
    this.eventEmitter.emit(['setHeatmapSettings', this.heatmapSettings]);
  }

  resetParams() {
    Reflect.ownKeys(this.state.heatmap.params).forEach((param): void => {
      this.eventEmitter.emit(['setHeatmapParams', param, this.state.heatmap.props[param]]);
      this.heatmap.setProps({ [param]: this.state.heatmap.props[param] });
    });
  }

  resetSettings() {
    this.map.setBearing(this.state.heatmap.settings.bearing);
    this.map.setCenter(this.state.heatmap.settings.center);
    this.map.setPitch(this.state.heatmap.settings.pitch);
    this.map.setZoom(this.state.heatmap.settings.zoom);
  }
}
