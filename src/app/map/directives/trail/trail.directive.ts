import { Directive, HostListener } from '@angular/core';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Directive({
  selector: '[appTrailChange]'
})
export class TrailDirective {
  constructor(private eventEmitterService: EventEmitterService) {}

  @HostListener('change', ['$event'])
  /* pan and zoom to selected trail */
  onChange(evt: MouseEvent): void {
    evt.stopPropagation();
    const trail: string = (evt as any).target.value;
    this.eventEmitterService.eventEmitter.emit(['selectTrail', trail]);
  }
}
