import { Directive, ElementRef, OnInit } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Directive({
  selector: '[appLayerClick], [appIconClick]'
})
export class LayerElementDirective implements OnInit {
  constructor(private el: ElementRef, private eventEmitterService: EventEmitterService) {}

  ngOnInit(): void {
    /* RxJS click handler subscription for the selectors above */
    const onClick: Subscription = fromEvent(this.el.nativeElement, 'click').subscribe({
      next: (evt: MouseEvent): void => {
        evt.stopPropagation();
        const id: string = (evt as any).target.id.split('-')[0];
        this.eventEmitterService.eventEmitter.emit(['selectLayer', id]);
      },
      error: (err: Error): void => {
        console.error('onClick Failed:\n', err);
      },
      complete: (): void => {
        onClick.unsubscribe();
      }
    });
  }
}
