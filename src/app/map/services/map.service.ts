import { EventEmitter, Injectable } from '@angular/core';
import { Layer, LngLatBoundsLike, Map, MapLayerMouseEvent, NavigationControl } from 'mapbox-gl';

import { MapSettings } from '@map/interfaces/map-settings';
import { Store } from '@store/interfaces/store';
import { Trail } from '@map/interfaces/trail';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private eventEmitter: EventEmitter<any>;
  private map: Map;
  private mapSettings: MapSettings;

  state: Store;

  constructor(private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
    this.mapSettings = {
      bearing: null,
      bounds: null,
      center: null,
      pitch: null,
      style: null,
      zoom: null
    };
  }

  loadMap(): void {
    this.map = new Map(this.state.map.settings)
      .addControl(new NavigationControl(), this.state.map.navigationControl.position)
      .on('click', (): void => {
        if (this.state.popup.layer.active) {
          this.eventEmitter.emit(['removePopup']);
        }
      })
      .on('load', (): void => {
        this.eventEmitter.emit(['loadFeatures']);
        this.addHillShade();
      })
      .on('idle', (): void => {
        this.setSettings();
      });
  }
  /* add DEM hillshade to 'outdoors' map style */
  private addHillShade(): void {
    if (this.state.map.styles[this.state.map.styles.active].id === this.state.map.styles.outdoors.id) {
      this.eventEmitter.emit(['setHillShadeActive']);
      this.map.addSource(this.state.map.hillshade.source, this.state.map.hillshade.layer);
      this.addLayer(this.state.map.hillshade as Layer);
    }
  }

  addLayer(layer: Layer): void {
    if (layer.source) {
      this.map.addLayer(layer, (layer as any).index);
      this.setLayerVisibility(layer);
    }
  }

  setLayerVisibility(layer: any): void {
    layer.active
      ? this.map.setLayoutProperty(layer.id, 'visibility', 'visible')
      : this.map.setLayoutProperty(layer.id, 'visibility', 'none');

    if (layer.active && layer.id === 'biosphere') {
      this.setLayerEventHandlers(layer.id);
    }
  }

  private setLayerEventHandlers(id: string): void {
    this.map
      .on('click', id, (evt: MapLayerMouseEvent): void => {
        if (!this.state.popup.layer.active) {
          this.eventEmitter.emit(['addLayerPopup', evt]);
        }
      })
      .on('mouseenter', id, (): void => {
        this.map.getCanvas().style.cursor = 'pointer';
      })
      .on('mouseleave', id, (): void => {
        this.map.getCanvas().style.cursor = '';

        if (this.state.popup.layer.active) {
          this.eventEmitter.emit(['removePopup']);
        }
      });
  }

  setMapStyle(): void {
    this.eventEmitter.emit(['setMapStyleActive']);
    this.map.setStyle(this.state.map.styles[this.state.map.styles.active].url);
    /* add hillshade & layers after 1 sec delay to set map style */
    setTimeout((): void => {
      this.state.map.styles[this.state.map.styles.active].id === this.state.map.styles.outdoors.id
        ? this.addHillShade()
        : this.eventEmitter.emit(['setHillShadeActive']);

      this.eventEmitter.emit(['addLayers']);
    }, 1000);
  }

  private setSettings(): void {
    this.mapSettings.bearing = this.map.getBearing();
    this.mapSettings.bounds = this.map.getBounds().toArray() as LngLatBoundsLike;
    this.mapSettings.center = this.map.getCenter().toArray();
    this.mapSettings.pitch = this.map.getPitch();
    this.mapSettings.style = this.state.map.styles[this.state.map.styles.active].url;
    this.mapSettings.zoom = this.map.getZoom();
    this.eventEmitter.emit(['setMapSettings', this.mapSettings]);
  }

  addToMap(feature: any): void {
    feature.addTo(this.map);
  }

  flyTo(trail: Trail): void {
    this.map.flyTo({
      center: trail.center,
      zoom: trail.zoom
    });
  }
}
