import { EventEmitter, Injectable } from '@angular/core';
import { Feature, Point } from 'geojson';
import { LngLatLike, MapLayerMouseEvent, Popup } from 'mapbox-gl';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  private eventEmitter: EventEmitter<any>;
  private popup: Popup;

  constructor(private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
    this.popup = new Popup({
      closeButton: false,
      offset: 5
    });
  }

  addLayerPopup(evt: MapLayerMouseEvent): void {
    this.popup
      .setHTML(
        `<div class="bold">${evt.features[0].properties.name}</div>
         <div>${evt.features[0].properties.description}</div>`
      )
      .setLngLat(evt.lngLat);

    this.eventEmitter.emit(['addToMap', this.popup]);
    this.eventEmitter.emit(['setLayerPopupActive']);
  }

  addMarkerPopup(layer: string, feature: Feature): void {
    this.popup.setHTML(
      `<div class="bold">${feature.properties.name}</div>
       <div>${feature.properties.description}</div>`
    );

    layer === 'trails'
      ? this.popup.setLngLat([feature.properties.lng, feature.properties.lat] as LngLatLike)
      : this.popup.setLngLat((feature.geometry as Point).coordinates as LngLatLike);

    this.eventEmitter.emit(['addToMap', this.popup]);
    this.eventEmitter.emit(['setMarkerPopupActive', layer, feature.properties.name]);
  }

  removePopup(layer: string): void {
    this.popup.remove();

    layer
      ? this.eventEmitter.emit(['setMarkerPopupActive', null, null])
      : this.eventEmitter.emit(['setLayerPopupActive']);
  }
}
