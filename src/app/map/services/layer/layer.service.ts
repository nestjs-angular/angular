import { Injectable } from '@angular/core';

import { Layer } from '@map/interfaces/layer';

@Injectable({
  providedIn: 'root'
})
export class LayerService {
  private layersHash: { biosphere: number | null; trails: number | null };

  layers: Layer[];

  constructor() {
    this.layers = [];
    this.layersHash = {
      biosphere: null,
      trails: null
    };
  }

  setLayerActive(id: string): void {
    const i: number = this.layersHash[id];

    this.layers[i].active = !this.layers[i].active;
    this.layers[i].active
      ? (this.layers[i].layout.visibility = 'visible')
      : (this.layers[i].layout.visibility = 'none');
  }

  setLayers(layer: Layer): void {
    this.layers.push(layer);
    this.layersHash[layer.id] = this.layers.length - 1;
  }
}
