import { TestBed } from '@angular/core/testing';

import { LayerUIService } from './layer-ui.service';

describe('LayerUIService', () => {
  let service: LayerUIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LayerUIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
