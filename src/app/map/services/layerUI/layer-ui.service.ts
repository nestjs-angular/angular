import { EventEmitter, Injectable } from '@angular/core';

import { LayerElement } from '@map/interfaces/layer-element';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class LayerUIService {
  private eventEmitter: EventEmitter<any>;

  constructor(private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  selectLayer(layer: LayerElement): void {
    switch (layer.id) {
      case 'satellite':
        /* hide active markers when changing map styles for aesthetic purposes */
        this.eventEmitter.emit(['showMarkers']);
        /* toggle between 'outdoors' and 'satellite' map styles (basemaps) */
        this.eventEmitter.emit(['setMapStyle']);
        /* show active markers when changing map styles for aesthetic purposes */
        setTimeout((): void => this.eventEmitter.emit(['showMarkers']), 1200);
        break;

      case 'biosphere':
      case 'trails':
        this.eventEmitter.emit(['setLayerActive', layer.id]);
        this.eventEmitter.emit(['setLayerVisibility', layer]);

        if (layer.id === 'trails') {
          this.eventEmitter.emit(['toggleMarkers', layer.id]);
        }
        break;

      case 'office':
      case 'places':
        this.eventEmitter.emit(['toggleMarkers', layer.id]);
        break;

      case 'heatmap':
        this.eventEmitter.emit(['setRoutePath', '/heatmap']);
        break;

      case 'logout':
        this.eventEmitter.emit(['logout']);
        break;
    }
  }
}
