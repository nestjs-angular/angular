import { EventEmitter, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { FeatureCollection, Feature, Point } from 'geojson';
import { LngLatLike, Marker } from 'mapbox-gl';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class MarkerService {
  private renderer: Renderer2;
  private eventEmitter: EventEmitter<any>;
  private markers: any[];
  private markersHash: { office: number | null; places: number | null; trails: number | null };

  constructor(private rendererFactory: RendererFactory2, private eventEmitterService: EventEmitterService) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
    this.eventEmitter = this.eventEmitterService.eventEmitter;
    this.markers = [];
    this.markersHash = {
      office: null,
      places: null,
      trails: null
    };
  }

  showMarkers(): void {
    this.markers.forEach((markers: Marker[]): void => {
      markers.forEach((marker: Marker): boolean => {
        const el: any = marker.getElement();

        if (!el.active && !el.hidden) {
          return true;
        }

        el.active = !el.active;
        el.hidden = !el.hidden;

        if (!el.active) {
          marker.remove();
        }

        if (!el.hidden) {
          this.eventEmitter.emit(['addToMap', marker]);
        }

        return true;
      });
    });
  }

  toggleMarkers(id: string): void {
    this.eventEmitter.emit(['setMarkerActive', id]);

    this.markers[this.markersHash[id]].forEach((marker: Marker): void => {
      const el: any = marker.getElement();

      el.active ? marker.remove() : this.eventEmitter.emit(['addToMap', marker]);
      el.active = !el.active;
    });
  }
  /* create individual marker elements and add mouse event handlers */
  setMarkers(fc: FeatureCollection, id: string): void {
    const markers: Marker[] = [];

    fc.features.forEach((feature: Feature): void => {
      const el: any = this.renderer.createElement('div');

      el.className = `${id}-marker`;
      el.active = false;
      el.hidden = false;

      el.addEventListener('mouseenter', (): void => {
        this.eventEmitter.emit(['addMarkerPopup', id, feature]);
      });
      el.addEventListener('mouseleave', (): void => {
        this.eventEmitter.emit(['removePopup', id]);
      });

      switch (id) {
        case 'office':
        case 'places':
          markers.push(new Marker(el).setLngLat((feature.geometry as Point).coordinates as LngLatLike));
          break;

        case 'trails':
          markers.push(new Marker(el).setLngLat([feature.properties.lng, feature.properties.lat] as LngLatLike));
          break;
      }
    });

    this.markers.push(markers);
    this.markersHash[id] = this.markers.length - 1;
  }
}
