import { LngLatBoundsLike } from 'mapbox-gl';

export interface MapSettings {
  bearing: number | null;
  bounds: LngLatBoundsLike | null;
  center: number[] | null;
  pitch: number | null;
  style: string | null;
  zoom: number | null;
}
