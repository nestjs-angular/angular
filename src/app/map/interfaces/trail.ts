import { LngLatLike } from 'mapbox-gl';

export interface Trail {
  active: boolean;
  center: LngLatLike;
  name: string;
  zoom: number;
}
