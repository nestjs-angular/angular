import { FeatureCollection } from 'geojson';

export interface Layer {
  active: boolean;
  data: FeatureCollection;
  'fill-color': string;
  'fill-opacity': number;
  'fill-outline-color': string;
  id: string;
  layout: any;
  'line-color': string;
  'line-width': number;
  paint: any;
  source: any;
  type: any;
  visibility: string;
}
