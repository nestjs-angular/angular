export interface LayerElement {
  active?: boolean;
  class?: string;
  height: number;
  icon_id: string;
  id: string;
  name: string;
  src: string;
  width: number;
}
