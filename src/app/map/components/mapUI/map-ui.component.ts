import { Component, OnInit } from '@angular/core';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-map-ui',
  styleUrls: ['map-ui.component.scss'],
  templateUrl: 'map-ui.component.html'
})
export class MapUIComponent implements OnInit {
  constructor(public storeService: StoreService) {}

  ngOnInit() {}
}
