import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconUIComponent } from '../iconUI/icon-ui.component';
import { LayerUIComponent } from '../layerUI/layer-ui.component';
import { MapUIComponent } from './map-ui.component';
import { TrailUIComponent } from '../trailUI/trail-ui.component';

describe('MapUIComponent', () => {
  let component: MapUIComponent;
  let fixture: ComponentFixture<MapUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconUIComponent, LayerUIComponent, MapUIComponent, TrailUIComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => expect(component).toBeTruthy());
});
