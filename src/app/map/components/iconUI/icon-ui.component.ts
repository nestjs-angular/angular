import { Component, OnInit } from '@angular/core';
import { state, style, trigger } from '@angular/animations';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-icon-ui',
  styleUrls: ['icon-ui.component.scss'],
  templateUrl: 'icon-ui.component.html',
  animations: [
    trigger('visibility', [
      state(
        'hidden',
        style({
          opacity: 0
        })
      ),
      state(
        'visible',
        style({
          opacity: 1
        })
      )
    ])
  ]
})
export class IconUIComponent implements OnInit {
  visible = 'hidden';

  constructor(public storeService: StoreService) {}

  ngOnInit(): void {
    setTimeout((): string => (this.visible = 'visible'), 1100);
  }
}
