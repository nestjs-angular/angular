import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-layer-ui',
  styleUrls: ['layer-ui.component.scss'],
  templateUrl: 'layer-ui.component.html',
  animations: [
    trigger('fade', [
      state(
        'fade-out',
        style({
          opacity: 0,
          transform: 'translateX(0) scale(0)'
        })
      ),
      state(
        'fade-in',
        style({
          opacity: 1,
          transform: 'translateX(0) scale(1)'
        })
      ),
      transition('fade-out => fade-in', animate(1000))
    ])
  ]
})
export class LayerUIComponent implements OnInit {
  fade = 'fade-out';

  constructor(public storeService: StoreService) {}

  ngOnInit(): void {
    setTimeout((): string => (this.fade = 'fade-in'));
  }
}
