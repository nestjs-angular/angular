import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrailUIComponent } from './trail-ui.component';

describe('TrailUIComponent', () => {
  let component: TrailUIComponent;
  let fixture: ComponentFixture<TrailUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TrailUIComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrailUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => expect(component).toBeTruthy());
});
