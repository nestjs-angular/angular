import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AuthenticationComponent } from '@access/authentication/components/authentication.component';
import { HeatmapComponent } from '@heatmap/components/heatmap.component';
import { MapComponent } from '@map/components/map.component';

import { LoginGuard } from '@access/authorization/guards/login/login.guard';
import { SessionGuard } from '@access/authorization/guards/session/session.guard';

const routes: Routes = [
  { path: '', component: AuthenticationComponent, canActivate: [LoginGuard] },
  /* lazy module loading not possible - async map instantiation fails */
  { path: 'map', component: MapComponent, canActivate: [SessionGuard] },
  { path: 'heatmap', component: HeatmapComponent, canActivate: [SessionGuard] }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
