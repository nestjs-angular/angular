import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from '@app/components/app.component';
import { AuthenticationComponent } from '@access/authentication/components/authentication.component';
import { HeaderComponent } from '@app/components/header/header.component';
import { HeatmapComponent } from '@heatmap/components/heatmap.component';
import { HeatmapUIComponent } from '@heatmap/components/heatmapUI/heatmap-ui.component';
import { HexagonUIComponent } from '@heatmap/components/hexagonUI/hexagon-ui.component';
import { IconUIComponent } from '@map/components/iconUI/icon-ui.component';
import { LayerUIComponent } from '@map/components/layerUI/layer-ui.component';
import { MapComponent } from '@map/components/map.component';
import { MapUIComponent } from '@map/components/mapUI/map-ui.component';
import { SplashScreenComponent } from '@app/components/splashScreen/splash-screen.component';
import { TrailUIComponent } from '@map/components/trailUI/trail-ui.component';

import { LayerElementDirective } from '@map/directives/layerElement/layer-element.directive';
import { TrailDirective } from '@map/directives/trail/trail.directive';

@NgModule({
  declarations: [
    AppComponent,
    AuthenticationComponent,
    HeaderComponent,
    HeatmapComponent,
    HeatmapUIComponent,
    HexagonUIComponent,
    IconUIComponent,
    LayerUIComponent,
    MapComponent,
    MapUIComponent,
    SplashScreenComponent,
    TrailUIComponent,
    LayerElementDirective,
    TrailDirective
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: (): string => {
          return sessionStorage.getItem('accessToken');
        },
        blacklistedRoutes: ['/auth/login', '/auth/register'],
        throwNoTokenError: true
      }
    })
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
