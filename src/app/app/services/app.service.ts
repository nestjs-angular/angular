import { EventEmitter, Injectable } from '@angular/core';

import { AuthorizationService } from '@access/authorization/services/authorization.service';
import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';
import { StoreService } from '@store/services/store.service';
import { SubscriptionService } from '@core/services/subscription/subscription.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private eventEmitter: EventEmitter<any>;

  constructor(
    private authorizationService: AuthorizationService,
    private eventEmitterService: EventEmitterService,
    private storeService: StoreService,
    private subscriptionService: SubscriptionService
  ) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  loadApp(): void {
    this.createStore();
    this.setSubscriptions();
    this.loadData();
    this.loadMap();
  }

  private createStore(): void {
    this.storeService.createStore();
  }

  private setSubscriptions(): void {
    this.subscriptionService.setSubscriptions();
  }

  private loadData(): void {
    this.eventEmitter.emit(['loadData']);
  }

  private async loadMap(): Promise<void> {
    await this.authorizationService.getMapboxAccessToken();
    /* refer to setRoutes subscription */
    this.eventEmitter.emit(['setRoutePath', '/map']);
  }
}
