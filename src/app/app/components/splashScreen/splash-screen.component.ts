import { Component, OnInit } from '@angular/core';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-splash-screen',
  styleUrls: ['splash-screen.component.scss'],
  templateUrl: 'splash-screen.component.html'
})
export class SplashScreenComponent implements OnInit {
  constructor(public storeService: StoreService) {}

  ngOnInit() {}
}
